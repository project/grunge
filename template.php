<?php

/**
* Breadcrumb modification
*/
function grunge_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb"><span class="sep">' . t('You are here &raquo;') . '</span>' . implode(' &raquo; ', $breadcrumb) . '</div>';
  }
}
